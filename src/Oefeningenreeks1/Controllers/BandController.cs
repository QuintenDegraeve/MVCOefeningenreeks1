﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyHobbies;

namespace Oefeningenreeks1.Controllers
{
    public class BandController : Controller
    {
        private List<Band> bands;
        public BandController()
        {
            bands = BandInitializer.StartUp();
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Lijst()
        {
            ViewBag.Bands = bands;
            return View();
        }

        public IActionResult LijstMetLeden()
        {
            ViewBag.Bands = bands;
            return View();
        }

        public IActionResult Maak(string naam, int jaar)
        {
            bands.Add(new Band() { Naam=naam, Jaar=jaar });
            ViewBag.Bands = bands;
            return View("Lijst");
        }

        public IActionResult JSLijst()
        {
            return Json(bands);
        }
    }
}
